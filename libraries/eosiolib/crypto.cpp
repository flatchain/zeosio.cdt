/**
 *  @file
 *  @copyright defined in eos/LICENSE
 */
#include "core/eosio/crypto.hpp"
#include "core/eosio/datastream.hpp"

extern "C" {
   struct __attribute__((aligned (16))) capi_checksum160 { uint8_t hash[20]; };
   struct __attribute__((aligned (16))) capi_checksum256 { uint8_t hash[32]; };
   struct __attribute__((aligned (16))) capi_checksum512 { uint8_t hash[64]; };
   struct __attribute__((aligned (16))) capi_cipher128 { uint8_t data[128]; };
   struct __attribute__((aligned (16))) capi_proof675 { uint8_t data[675]; };
   __attribute__((eosio_wasm_import))
   void assert_sha256( const char* data, uint32_t length, const capi_checksum256* hash );

   __attribute__((eosio_wasm_import))
   void assert_sha1( const char* data, uint32_t length, const capi_checksum160* hash );
   
   __attribute__((eosio_wasm_import))
   void assert_sha512( const char* data, uint32_t length, const capi_checksum512* hash );

   __attribute__((eosio_wasm_import))
   void assert_ripemd160( const char* data, uint32_t length, const capi_checksum160* hash );

   __attribute__((eosio_wasm_import))
   void sha256( const char* data, uint32_t length, capi_checksum256* hash );

   __attribute__((eosio_wasm_import))
   void sha1( const char* data, uint32_t length, capi_checksum160* hash );

   __attribute__((eosio_wasm_import))
   void sha512( const char* data, uint32_t length, capi_checksum512* hash );

   __attribute__((eosio_wasm_import))
   void ripemd160( const char* data, uint32_t length, capi_checksum160* hash );

   __attribute__((eosio_wasm_import))
   int recover_key( const capi_checksum256* digest, const char* sig, 
                    size_t siglen, char* pub, size_t publen );

   __attribute__((eosio_wasm_import))
   void assert_recover_key( const capi_checksum256* digest, const char* sig, 
                            size_t siglen, const char* pub, size_t publen );

	__attribute__((eosio_wasm_import))
	void assert_cipher_ecdsa_signature( const char* eq_para, size_t eq_len,
                                       const char* from_pub_para, size_t fp_len,
                                       const char* to_pub_para, size_t tp_len,
                                       const char* sig, size_t sig_len);

   __attribute__((eosio_wasm_import))
   void assert_cipher_equal_prove(const char* cipher_balance, size_t cb_len,
                                  const char* eq_para, size_t eq_len,
                                  const char* pub_para, size_t pp_len);

   __attribute__((eosio_wasm_import))
   void assert_cipher_valid_prove(const char* prove_para, size_t pv_len, const char* pub_para, size_t pp_len);

   __attribute__((eosio_wasm_import))
   void assert_cipher_encrypt(const char* random, size_t rand_len, unsigned int value, const char* pub_para, size_t pp_len, const char* ciphertext, size_t ct_len);

   __attribute__((eosio_wasm_import))
   void cipher_add(const char* cipher1, size_t len1, const char* cipher2, size_t len2, capi_cipher128* out);

   __attribute__((eosio_wasm_import))
   void prove_bulletproof( uint64_t value, const unsigned char* blind, size_t blindlen, unsigned char* proof, size_t plen);

   __attribute__((eosio_wasm_import))
   void assert_verify_bulletproof(const unsigned char* commit, size_t commitlen, const unsigned char* proof, size_t prooflen);

   __attribute__((eosio_wasm_import))
   void pedersen_commit(uint64_t value, const unsigned char* blind, size_t blindlen, unsigned char* commit, size_t commitlen);
}

namespace eosio {

   void assert_sha256( const char* data, uint32_t length, const eosio::checksum256& hash ) {
      auto hash_data = hash.extract_as_byte_array();
      ::assert_sha256( data, length, reinterpret_cast<const ::capi_checksum256*>(hash_data.data()) );
   }

   void assert_sha1( const char* data, uint32_t length, const eosio::checksum160& hash ) {
      auto hash_data = hash.extract_as_byte_array();
      ::assert_sha1( data, length, reinterpret_cast<const ::capi_checksum160*>(hash_data.data()) );
   }

   void assert_sha512( const char* data, uint32_t length, const eosio::checksum512& hash ) {
      auto hash_data = hash.extract_as_byte_array();
      ::assert_sha512( data, length, reinterpret_cast<const ::capi_checksum512*>(hash_data.data()) );
   }

   void assert_ripemd160( const char* data, uint32_t length, const eosio::checksum160& hash ) {
      auto hash_data = hash.extract_as_byte_array();
      ::assert_ripemd160( data, length, reinterpret_cast<const ::capi_checksum160*>(hash_data.data()) );
   }

   eosio::checksum256 sha256( const char* data, uint32_t length ) {
      ::capi_checksum256 hash;
      ::sha256( data, length, &hash );
      return {hash.hash};
   }

   eosio::checksum160 sha1( const char* data, uint32_t length ) {
      ::capi_checksum160 hash;
      ::sha1( data, length, &hash );
      return {hash.hash};
   }

   eosio::checksum512 sha512( const char* data, uint32_t length ) {
      ::capi_checksum512 hash;
      ::sha512( data, length, &hash );
      return {hash.hash};
   }

   eosio::checksum160 ripemd160( const char* data, uint32_t length ) {
      ::capi_checksum160 hash;
      ::ripemd160( data, length, &hash );
      return {hash.hash};
   }

   eosio::public_key recover_key( const eosio::checksum256& digest, const eosio::signature& sig ) {
      auto digest_data = digest.extract_as_byte_array();

      char sig_data[70];
      eosio::datastream<char*> sig_ds( sig_data, sizeof(sig_data) );
      auto sig_begin = sig_ds.pos();
      sig_ds << sig;

      char pubkey_data[38];
      size_t pubkey_size = ::recover_key( reinterpret_cast<const capi_checksum256*>(digest_data.data()),
                                          sig_begin, (sig_ds.pos() - sig_begin),
                                          pubkey_data, sizeof(pubkey_data) );
      eosio::datastream<char*> pubkey_ds( pubkey_data, pubkey_size );
      eosio::public_key pubkey;
      pubkey_ds >> pubkey;
      return pubkey;
   }

   void assert_recover_key( const eosio::checksum256& digest, const eosio::signature& sig, const eosio::public_key& pubkey ) {
      auto digest_data = digest.extract_as_byte_array();

      char sig_data[70];
      eosio::datastream<char*> sig_ds( sig_data, sizeof(sig_data) );
      auto sig_begin = sig_ds.pos();
      sig_ds << sig;

      char pubkey_data[38];
      eosio::datastream<char*> pubkey_ds( pubkey_data, sizeof(pubkey_data) );
      auto pubkey_begin = pubkey_ds.pos();
      pubkey_ds << pubkey;

      ::assert_recover_key( reinterpret_cast<const capi_checksum256*>(digest_data.data()),
                            sig_begin, (sig_ds.pos() - sig_begin),
                            pubkey_begin, (pubkey_ds.pos() - pubkey_begin) );
   }

void assert_cipher_ecdsa_signature(const char* eq_para, size_t eq_len,
                                       const char* from_pub_para, size_t fp_len,
                                       const char* to_pub_para, size_t tp_len,
                                       const char* sig, size_t sig_len) {
         ::assert_cipher_ecdsa_signature( eq_para, eq_len,
                                          from_pub_para, fp_len,
                                          to_pub_para, tp_len,
                                          sig, sig_len);
   }

void assert_cipher_equal_prove(const char* cipher_balance, size_t cb_len,
                                  const char* eq_para, size_t eq_len,
                                  const char* pub_para, size_t pp_len) {
   ::assert_cipher_equal_prove(cipher_balance, cb_len, eq_para, eq_len, pub_para, pp_len);
}

void assert_cipher_valid_prove(const char* prove_para, size_t pv_len, const char* pub_para, size_t pp_len) {
   ::assert_cipher_valid_prove(prove_para, pv_len, pub_para, pp_len);
}

void assert_cipher_encrypt(const char* random, size_t rand_len, unsigned int value, const char* pub_para, size_t pp_len, const char* ciphertext, size_t ct_len) {
   ::assert_cipher_encrypt( random, rand_len, value, pub_para, pp_len, ciphertext, ct_len);
}

eosio::cipher128 cipher_add(const char* cipher1, size_t len1, const char* cipher2, size_t len2) {
   ::capi_cipher128 cipher;
   ::cipher_add( cipher1, len1, cipher2, len2, &cipher );

   eosio::cipher128 c;
   std::memcpy (c.data(),cipher.data, 128);
   return c;
}

void prove_bulletproof( uint64_t value, const unsigned char* blind, size_t blindlen, unsigned char *proof, size_t plen) {
   ::prove_bulletproof(value, blind, blindlen, proof, plen);
}

void assert_verify_bulletproof(const unsigned char* commit, size_t commitlen, const unsigned char* proof, size_t prooflen) {
   ::assert_verify_bulletproof(commit, commitlen, proof, prooflen);
}

void pedersen_commit(uint64_t value, const unsigned char* blind, size_t blindlen, unsigned char* commit, size_t commitlen) {
   ::pedersen_commit(value, blind, blindlen, commit, commitlen);
}
}
